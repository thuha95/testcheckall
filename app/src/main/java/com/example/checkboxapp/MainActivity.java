package com.example.checkboxapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Adapter adapter;
    private List<Obj> objList = new ArrayList<>();
    private List<Obj> objListChoose = new ArrayList<>();
    private boolean isCheckAll = false;
    private int count = 0;
    ImageView imgView;
    TextView tv;
    RecyclerView rcv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initRcv();
        initData();

    }

    private void initRcv() {
        imgView = findViewById(R.id.imageCheck);
        tv = findViewById(R.id.tvNumberSelect);
        rcv = findViewById(R.id.fragment_dialog_confirm_pick_by_truck_list);
        adapter = new Adapter(objList);
//        initText();
        adapter.setmOnSelect(new Adapter.OnSelect() {
            @Override
            public void itemmSelect(boolean isClick, Obj item, int position) {
                if (item.isSelect && objListChoose.indexOf(item) == -1) {
                    objListChoose.add(item);
                } else if (!item.isSelect) {
                    objListChoose.remove(item);
                }

                if (isClick) {
                    initCheckAll(!(objListChoose.size() < objList.size()));
                    adapter.notifyItemChanged(position, Adapter.UPDATE_SELECTED);
                }
                initText();

            }
        });

        rcv.setLayoutManager(new LinearLayoutManager(this));
        rcv.setAdapter(adapter);
        setEvenSelect();


    }

    private void initText() {
        if (objListChoose.size() > 0) {
            tv.setText("Da chon: " + objListChoose.size() + " dh");
        } else {
            tv.setText("Chon don hang");
        }
//        if (objListChoose.size() == objList.size()) {
//            isCheckAll = true;
//        } else {
//            isCheckAll = false;
//        }
//        initCheckAll(isCheckAll);
    }

    private void setEvenSelect() {
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initCheckAll(!adapter.isCheckAll());
                adapter.notifyDataSetChanged();
            }
        });

    }

    private void initCheckAll(boolean isCheckAll) {
        if (isCheckAll) {
            imgView.setImageResource(R.drawable.ic_check_box_black_checked);
            adapter.checkAll(true);
            tv.setText("Chon tat ca " + objList.size() + "DDH");
        } else {
            imgView.setImageResource(R.drawable.ic_check_box_black_unchecked);
            adapter.checkAll(false);
            tv.setText("Chon " + objList.size() + " DH");
        }
    }

    private void initData() {
        for (int i = 0; i < 10; i++) {
            Obj obj = new Obj(i + "");
            objList.add(obj);
        }
        adapter.notifyDataSetChanged();
    }
}
