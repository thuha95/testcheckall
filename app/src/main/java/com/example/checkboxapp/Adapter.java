package com.example.checkboxapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    public static final int UPDATE_SELECTED =  1;
    List<Obj> objList = new ArrayList<>();
    private boolean isCheckAll;

    private OnSelect mOnSelect;

    public void setmOnSelect(OnSelect mOnSelect) {
        this.mOnSelect = mOnSelect;
    }

    public Adapter(List<Obj> list) {
        objList = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Obj item = objList.get(position);
        holder.textView.setText("Number: " + item.number);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.isSelect = !item.isSelect;
                if (mOnSelect != null) {
                    mOnSelect.itemmSelect(true, item, position);
                }
            }
        });

        if (isCheckAll && !item.isSelect) {
            item.setSelect(true);
            if (mOnSelect != null) {
                mOnSelect.itemmSelect(false, item, position);
            }
        } else if (!isCheckAll && item.isSelect) {
            item.setSelect(false);
            if (mOnSelect != null) {
                mOnSelect.itemmSelect(false, item, position);
            }
        }

        if (item.isSelect) {
            holder.imageView.setImageResource(R.drawable.ic_check_box_black_checked);
        } else {
            holder.imageView.setImageResource(R.drawable.ic_check_box_black_unchecked);
        }
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        Obj item = objList.get(position);
        if (payloads.size() > 0){
            if(payloads.get(0) instanceof  Integer) {
                int update = (int) payloads.get(0);
                if(update == UPDATE_SELECTED){
                    if (item.isSelect) {
                        holder.imageView.setImageResource(R.drawable.ic_check_box_black_checked);
                    } else {
                        holder.imageView.setImageResource(R.drawable.ic_check_box_black_unchecked);
                    }
                }
                return;
            }
        }
        super.onBindViewHolder(holder, position, payloads);
    }

    public void checkAll(boolean isCheckAll) {
        this.isCheckAll = isCheckAll;
    }

    public boolean isCheckAll() {
        return isCheckAll;
    }

    @Override
    public int getItemCount() {
        return objList == null ? 0 : objList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_check);
            textView = itemView.findViewById(R.id.tvText);
        }
    }

    public interface OnSelect {
        void itemmSelect(boolean isClick, Obj item, int position);

    }
}
